package com.milic.chatapp.service;

import com.milic.chatapp.exception.DataNotFoundException;
import com.milic.chatapp.model.Message;
import com.milic.chatapp.repository.MessageRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Sort;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class MessageServiceTest {

    @Mock
    private MessageRepository messageRepository;

    @Test(expected = DataNotFoundException.class)
    public void get_messages_not_found() {
        //given
        Mockito.when(messageRepository.findAll(ArgumentMatchers.any(Sort.class))).thenReturn(new LinkedList<>());
        MessageService messageService = new MessageService(messageRepository);

        //when
        messageService.findSortedByCreatedDateAsc();
    }

    @Test
    public void get_messages_successfully() {
        //given
        List<Message> expectedMessages = Arrays.asList(new Message(), new Message());
        Mockito.when(messageRepository.findAll(ArgumentMatchers.any(Sort.class))).thenReturn(expectedMessages);
        MessageService messageService = new MessageService(messageRepository);

        //when
        List<Message> messages = messageService.findSortedByCreatedDateAsc();

        //then
        Assert.assertEquals(messages.size(), expectedMessages.size());
    }
}
