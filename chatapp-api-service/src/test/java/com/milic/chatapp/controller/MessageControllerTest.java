package com.milic.chatapp.controller;

import com.milic.chatapp.controller.v1.api.MessageController;
import com.milic.chatapp.controller.v1.model.CreateMessageRequest;
import com.milic.chatapp.exception.DataNotFoundException;
import com.milic.chatapp.model.Message;
import com.milic.chatapp.service.MessageService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class MessageControllerTest {

    @Mock
    MessageService messageService;

    @InjectMocks
    MessageController messageController;

    @Test
    public void get_messages_successfully() {
        // given
        List<Message> expectedMessages = Collections.singletonList(getTestMessage());
        Mockito.when(messageService.findSortedByCreatedDateAsc()).thenReturn(expectedMessages);

        // when
        ResponseEntity<List<Message>> response = messageController.getMessages();

        // then
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
        Assert.assertEquals(expectedMessages, response.getBody());
    }

    @Test(expected = DataNotFoundException.class)
    public void get_messages_not_found() {
        // given
        Mockito.when(messageService.findSortedByCreatedDateAsc()).thenThrow(new DataNotFoundException("Data not found."));

        // when
        ResponseEntity<List<Message>> response = messageController.getMessages();
    }

    @Test
    public void create_message_successfully() {
        // given
        Message expectedMessage = getTestMessage();
        Mockito.when(messageService.createMessage(ArgumentMatchers.any(Message.class))).thenReturn(expectedMessage);

        // when
        ResponseEntity<Message> response = messageController.createMessage(getTestCreateMessageRequest());

        // then
        Assert.assertEquals(HttpStatus.CREATED, response.getStatusCode());
        Assert.assertEquals(expectedMessage, response.getBody());
    }

    private CreateMessageRequest getTestCreateMessageRequest() {
        return new CreateMessageRequest()
                .setContent("Hello.")
                .setUser("John");
    }

    private Message getTestMessage() {
        return new Message()
                .setContent("Hello.")
                .setUser("John");
    }
}
