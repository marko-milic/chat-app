package com.milic.chatapp.model;

import java.time.LocalDateTime;

public class User {

    private String username;
    private String password;
    private String email;
    private LocalDateTime createdDate;
    private LocalDateTime updatedDate;
}
