package com.milic.chatapp.controller.v1.api;

import com.milic.chatapp.controller.v1.mapper.MessageMapper;
import com.milic.chatapp.controller.v1.model.CreateMessageRequest;
import com.milic.chatapp.model.Message;
import com.milic.chatapp.service.MessageService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;


@RestController
@CrossOrigin
@RequestMapping(value = "/api/v1" + ControllerConstants.MESSAGE_URI_PATH, produces = MediaType.APPLICATION_JSON_VALUE)
@Api(value = "Chat Message REST API")
public class MessageController {

    private MessageService messageService;

    @Autowired
    public MessageController(MessageService messageService) {
        this.messageService = messageService;
    }

    @GetMapping
    @ApiOperation(value = "Get all messages chronologically sorted")
    public ResponseEntity<List<Message>> getMessages() {
        List<Message> messages = messageService.findSortedByCreatedDateAsc();

        return new ResponseEntity<>(messages, HttpStatus.OK);
    }

    @PostMapping
    @ApiOperation(value = "Create new message")
    public ResponseEntity<Message> createMessage(@Valid @RequestBody CreateMessageRequest createMessageRequest) {
        Message message = MessageMapper.toMessage(createMessageRequest);
        Message createdMessage = messageService.createMessage(message);

        return new ResponseEntity<>(createdMessage, HttpStatus.CREATED);
    }
}
