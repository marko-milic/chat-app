package com.milic.chatapp.controller.v1.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@JsonInclude(value = JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class CreateMessageRequest {

    @NotNull
    @NotEmpty
    private String content;

    @NotNull
    @NotEmpty
    private String user;

    public String getContent() {
        return content;
    }

    public CreateMessageRequest setContent(String content) {
        this.content = content;
        return this;
    }

    public String getUser() {
        return user;
    }

    public CreateMessageRequest setUser(String user) {
        this.user = user;
        return this;
    }
}
