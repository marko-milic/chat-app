import React from 'react'

class Chat extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isFetching: false,
            messages: [],
        }
    }

    componentDidMount() {
        this.setState({ ...this.state, isFetching: true });

        this.fetchMessages();
    }

    render() {
        const messages = this.state.messages;

        return (
            <div>
                <p>Chat Component</p>
                {messages.map(message => {
                    return <div key={message.id}>
                        {message.user} |  
                        {message.content} | 
                        {message.createdDate}
                    </div>
                })}
                <div></div>
            </div>
        )
    }

    fetchMessages = () => {
        fetch('http://localhost:8080/api/v1/messages', { method: 'GET' })
                .then(response => response.json())
                .then(data => {
                    this.setState({ ...this.state, messages: data });
                }).catch(e => {
                    console.log(e);
                });
    }
}

export default Chat;